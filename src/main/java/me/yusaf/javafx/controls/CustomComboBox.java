package me.yusaf.javafx.controls;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.scene.control.ComboBox;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;

public class CustomComboBox<T> extends ComboBox<T> {
	private ObservableList<T> allItems;
	private Predicate<T> filter;
	private Comparator<? super T> comparator;

	private List<KeyCodeCombination> ignorableKeys = new ArrayList<>();

	public CustomComboBox() {
		setEditable(true);
		setOnKeyReleased(event -> {
			if (ignorableKeys.stream().anyMatch(p -> p.match(event))) {
				return;
			}

			String text = getEditor().getText();
			if (text.length() > 0) {
				FilteredList<T> filtered = allItems.filtered(filter);
				SortedList<T> sorted = new SortedList<>(filtered);
				sorted.setComparator(comparator);
				setItems(sorted);
				show();
			}
			if (new KeyCodeCombination(KeyCode.BACK_SPACE).match(event)) {
				// When backspacing is done, then text could be empty, hence clear filter.
				if (text.length() == 0) {
					setItems(allItems);
				}
			} else if (new KeyCodeCombination(KeyCode.ESCAPE).match(event)) {
				hide();
			}
		});

		ignorableKeys.addAll(List.of(
				new KeyCodeCombination(KeyCode.UP),
				new KeyCodeCombination(KeyCode.DOWN),
				new KeyCodeCombination(KeyCode.LEFT),
				new KeyCodeCombination(KeyCode.RIGHT),
				new KeyCodeCombination(KeyCode.HOME),
				new KeyCodeCombination(KeyCode.TAB),
				new KeyCodeCombination(KeyCode.ENTER),
				new KeyCodeCombination(KeyCode.END)));
	}

	public CustomComboBox<T> setAllItems(ObservableList<T> allItems) {
		this.allItems = allItems;
		setItems(allItems);
		return this;
	}

	public void setFilterPredicate(Predicate<T> filterPredicate) {
		filter = filterPredicate;
	}

	public void setComparator(Comparator<? super T> comparator) {
		this.comparator = comparator;
	}
}
