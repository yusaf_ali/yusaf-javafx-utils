package me.yusaf.javafx.controls;

import java.util.logging.Logger;

import javafx.geometry.Pos;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyEvent;

/**
 * A TextField extension type. Only allows numeric values.<br>
 * Also adds a scroll listener on itself to increase or decrease value against scroll events.<br>
 * Effectively blocks scroll event by consuming it on bubbling phase.<br>
 * Parent control will not be able to receive this event.
 *
 * @author Yusaf Ali
 *
 */
public class NumberField extends TextField {
	private static final Logger logger = Logger.getLogger(NumberField.class.getName());

	private Runnable onScrollExtended;
	private Runnable onUpdate;
	private Number value;
	private Number oldValue;

	public NumberField() {
		this(0, 2);
	}

	public NumberField(Number number) {
		this(number, 2);
	}

	public NumberField(Number number, int rounding) {
		setAlignment(Pos.CENTER_RIGHT);
		getStyleClass().add("number-field");
		setText(number.doubleValue() + "");
		this.value = number;

		this.addEventFilter(KeyEvent.KEY_PRESSED, e -> {
			if (new KeyCodeCombination(KeyCode.ESCAPE).match(e)) {
				logger.finest("Escape was pressed!");
				value = oldValue;
				setText(value.toString());
				this.getParent().requestFocus();
			}
		});

		focusedProperty().addListener((obs, old, value) -> {
			if (value) {
				oldValue = this.value;
			} else {
				cancelEdit();
			}
		});

		textProperty().addListener((obs, old, value) -> {
			if (!value.isBlank()) {
				Double d = 0d;
				try {
					d = Double.parseDouble(value);
					this.value = d;
				} catch (NumberFormatException e) {
					setText(old);
				}
			}
		});

		setOnScroll((event) -> {
			if (!isNumber() || !isFocused() || !isEditable()) {
				return;
			}
			event.consume();
			double value = Double.parseDouble(getText());

			if (event.isControlDown()) {
				value += event.getDeltaY() > 0 ? 100 : -100;
			} else if (event.isShiftDown()) {
				value += event.getDeltaX() > 0 ? 10 : -10;
			} else if (event.isAltDown()) {
				value += event.getDeltaY() > 0 ? 0.1 : -0.1;
			} else {
				value += event.getDeltaY() > 0 ? 1 : -1;
			}

			String formatted = rounding > 0 ? String.format("%." + rounding + "f", value) : String.format("%1.0$d", value);
			setText(formatted);
			logger.finest("v: " + value + ", f: " + formatted);
			if (onUpdate != null) {
				onUpdate.run();
			}
			if (onScrollExtended != null) {
				onScrollExtended.run();
			}
			this.value = value;
		});
	}

	/**
	 * onScrollExtended will trigger after onUpdate.
	 *
	 * @param onScrollExtended
	 */
	public void setOnScrollExtended(Runnable onScrollExtended) {
		this.onScrollExtended = onScrollExtended;
	}

	/**
	 * onUpdate will trigger before onScrollExtended.
	 *
	 * @param onUpdate
	 */
	public void onUpdate(Runnable onUpdate) {
		this.onUpdate = onUpdate;
	}

	private boolean isNumber() {
		try {
			Double.parseDouble(getText());
			return true;
		} catch (NumberFormatException e) {
			logger.severe(e.getMessage());
		}
		return false;
	}

	public Double doubleValue() {
		return value.doubleValue();
	}

	public Float floatValue() {
		return value.floatValue();
	}

	public Long longValue() {
		return value.longValue();
	}

	public Integer intValue() {
		return value.intValue();
	}
}
