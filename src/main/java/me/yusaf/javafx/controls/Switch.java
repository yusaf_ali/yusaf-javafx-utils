package me.yusaf.javafx.controls;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.css.PseudoClass;
import javafx.geometry.HPos;
import javafx.scene.control.Label;
import javafx.scene.effect.Blend;
import javafx.scene.effect.BlendMode;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.shape.Circle;

public class Switch extends GridPane {
	private BooleanProperty checked = new SimpleBooleanProperty(false);

	private Circle knob = new Circle(10.0);
	private Label label = new Label();

	private String onText, offText;
	private static final String CSS_SWITCH = "switch";

	PseudoClass stateOn = PseudoClass.getPseudoClass("on");
	PseudoClass stateOff = PseudoClass.getPseudoClass("off");

	ColumnConstraints colLabel = new ColumnConstraints();
	ColumnConstraints colKnob = new ColumnConstraints();

	public Switch() {
		getStyleClass().add(CSS_SWITCH);
		getStylesheets().add("switch.css");
		knob.getStyleClass().add("knob");
		knob.radiusProperty()
				.bind(heightProperty()
						.divide(2)
						.add(knob.getStrokeWidth() * -1));

		colLabel.setFillWidth(true);
		colKnob.prefWidthProperty().bind(heightProperty());
		getColumnConstraints().addAll(colKnob, colLabel);
		setHgap(5);

		Blend blend = new Blend();
		blend.setMode(BlendMode.SRC_OVER);
		blend.setBottomInput(null);
		knob.setEffect(blend);

		GridPane.setHalignment(label, HPos.CENTER);
		colLabel.setHgrow(Priority.ALWAYS);

		setOnMouseClicked(e -> {
			checked.set(!checked.get());
			// System.out.println(this.getStyleClass().stream().collect(Collectors.joining(", ")));
			redraw();
		});

		redraw();
	}

	public Switch redraw() {
		getChildren().clear();

		if (checked.get()) {
			GridPane.setHalignment(label, HPos.CENTER);
			label.setText(onText);

			add(knob, 1, 0);
			add(label, 0, 0);

			pseudoClassStateChanged(stateOn, true);
			pseudoClassStateChanged(stateOff, false);

			getColumnConstraints().clear();
			getColumnConstraints().addAll(colLabel, colKnob);
		} else {
			GridPane.setHalignment(label, HPos.CENTER);
			label.setText(offText);

			add(knob, 0, 0);
			add(label, 1, 0);

			pseudoClassStateChanged(stateOn, false);
			pseudoClassStateChanged(stateOff, true);

			getColumnConstraints().clear();
			getColumnConstraints().addAll(colKnob, colLabel);
		}
		return this;
	}

	public Switch withYesNoText() {
		return onText("Yes").offText("No");
	}

	public Switch onText(String onText) {
		this.onText = onText;
		return this;
	}

	public Switch offText(String offText) {
		this.offText = offText;
		return this;
	}

	public Switch setOn() {
		this.checked.setValue(true);
		redraw();
		return this;
	}

	public Switch setOff() {
		this.checked.setValue(false);
		redraw();
		return this;
	}

	public Switch setValue(boolean isOn) {
		checked.set(isOn);
		redraw();
		return this;
	}

	public boolean isOn() {
		return checked.getValue();
	}

	public boolean isOff() {
		return !checked.getValue();
	}

	public void addListener(ChangeListener<Boolean> listener) {
		this.checked.addListener(listener);
	}
}
