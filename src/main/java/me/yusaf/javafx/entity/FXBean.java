package me.yusaf.javafx.entity;

import javafx.beans.property.BooleanProperty;

public interface FXBean {
	/**
	 * Create a boolean property like this<br>
	 * {@code BooleanProperty checkedProperty = new SimpleBooleanProperty();}<br>
	 * and return it from this method.
	 *
	 * @return
	 */
	BooleanProperty checkedProperty();
	BooleanProperty newProperty();
	BooleanProperty checkEditableProperty();

	@Deprecated
	default boolean isChanged() {
		throw new RuntimeException(this.getClass().getName() + "#isChanged not implemented!");
	}

	default void setChecked(boolean value) {
		checkedProperty().set(value);
	}

	default boolean isChecked() {
		return checkedProperty().getValue();
	}

	default boolean isCheckEditable() {
		return checkEditableProperty().get();
	}

	default void setCheckEditable(boolean editable) {
		checkEditableProperty().set(editable);
	}

	default boolean isNew() {
		return newProperty().get();
	}

	default void setNew(boolean isNew) {
		newProperty().set(isNew);
	}
}
