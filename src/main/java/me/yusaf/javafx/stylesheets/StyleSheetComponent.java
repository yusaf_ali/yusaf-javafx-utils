package me.yusaf.javafx.stylesheets;

import java.nio.file.Path;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javafx.scene.Parent;

public class StyleSheetComponent {
	public String name;
	public Class<?> componentClass;
	public List<String> stylesheets;
	public Set<String> imports;
	public Set<Path> stylesheetTempFiles = new HashSet<>();
	public Parent parent;
}
