package me.yusaf.javafx.tablecell;

import java.util.function.BiConsumer;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import me.yusaf.javafx.entity.FXBean;

public class ButtonTableCell<T extends FXBean> extends TableCell<T, String> {
	private Button button = new Button();
	// private Consumer<Row> r;

	public ButtonTableCell(String text) {
		this.setGraphic(null);
		this.setText(null);
		this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
		this.getStyleClass().add("custom-cell-button-container");
		setAlignment(Pos.CENTER);
		button.setText(text);
	}

	public Button getButton() {
		return button;
	}

	public ButtonTableCell<T> onAction(BiConsumer<T, Button> r) {
		// this.r = r;
		this.button.setOnAction(e -> {
			r.accept(getTableRow().getItem(), this.button);
		});
		return this;
	}

	@Override
	protected void updateItem(String item, boolean empty) {
		super.updateItem(item, empty);

		if (!empty) {
			setGraphic(button);
			// if (property != null) {
			// button.textProperty().unbindBidirectional(property);
			// }
			// property = (StringProperty) getTableColumn().getCellObservableValue(getIndex());
			// button.textProperty().bindBidirectional(property);

			// if (getTableRow() != null) {
			//
			// }
		} else {
			setGraphic(null);
		}
	}
}
