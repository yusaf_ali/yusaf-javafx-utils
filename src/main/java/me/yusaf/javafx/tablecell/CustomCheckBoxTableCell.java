package me.yusaf.javafx.tablecell;

import javafx.beans.property.BooleanProperty;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import me.yusaf.javafx.entity.FXBean;

public class CustomCheckBoxTableCell<T extends FXBean> extends TableCell<T, Boolean> {
	private CheckBox checkBox = new CheckBox();
	private BooleanProperty booleanProperty;

	public CustomCheckBoxTableCell() {
		getStyleClass().add("check-box-table-cell");
		setGraphic(checkBox);
		setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
		setEditable(true);
		checkBox.setFocusTraversable(false);
		checkBox.setAlignment(Pos.CENTER);
	}

	@Override
	protected void updateItem(Boolean item, boolean empty) {
		super.updateItem(item, empty);

		if (!isEmpty()) {
			if (booleanProperty != null) {
				checkBox.selectedProperty().unbindBidirectional(booleanProperty);
			}
			booleanProperty = (BooleanProperty) getTableColumn().getCellObservableValue(getIndex());

			setGraphic(checkBox);
			if (getTableRow() != null && getTableRow().getItem() != null) {
				checkBox.setDisable(!getTableRow().getItem().isCheckEditable());
			}

			if (booleanProperty != null) {
				checkBox.selectedProperty().bindBidirectional(booleanProperty);
			}
		} else {
			setText(null);
			setGraphic(null);
		}
	}

	public CheckBox getCheckBox() {
		return checkBox;
	}
}
