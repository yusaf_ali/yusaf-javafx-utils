package me.yusaf.javafx.utils;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;

public class AlertUtils {
	public static void warn(String message) {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setHeaderText(message);
		alert.showAndWait();
	}

	public static void warn(String message, String description) {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setHeaderText(message);
		alert.setContentText(description);
		alert.showAndWait();
	}

	public static boolean confirm(String message) {
		Alert alert = new Alert(AlertType.CONFIRMATION, message);
		alert.showAndWait();

		if (alert.getResult() != null && alert.getResult().getButtonData().equals(ButtonData.OK_DONE)) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean confirm(String title, String message, String description) {
		Alert alert = new Alert(AlertType.CONFIRMATION, description);
		alert.setTitle(title);
		alert.setHeaderText(message);
		alert.showAndWait();

		if (alert.getResult() != null && alert.getResult().getButtonData().equals(ButtonData.OK_DONE)) {
			return true;
		} else {
			return false;
		}
	}

	public static void info(String message) {
		Alert alert = new Alert(AlertType.INFORMATION, message);
		alert.show();
	}
}
