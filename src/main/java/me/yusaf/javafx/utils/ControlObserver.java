package me.yusaf.javafx.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javafx.collections.ListChangeListener.Change;
import javafx.scene.control.Control;
import javafx.scene.control.TextInputControl;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;

public class ControlObserver {
	private static final String INITIAL_VALUE = "initial-value";
	private List<Function<List<Control>, Boolean>> conditions = new ArrayList<>();
	private Control[] modifiables;
	private List<Control> modifiers;
	private boolean enableMode = true;
	private List<Pane> panes;

	private static final Logger logger = Logger.getLogger(ControlObserver.class.getName());

	@SafeVarargs
	public ControlObserver(Control... controls) {
		modifiables = controls;
	}

	public ControlObserver(List<Control> controls) {
		modifiables = new Control[controls.size()];
		for (int i = 0; i < controls.size(); i++)
			modifiables[i] = controls.get(i);
	}

	public ControlObserver modifiers(List<Control> modifiers) {
		this.modifiers = modifiers;
		modifiers.forEach(modifier -> attachListenerToModifier(modifier));
		return this;
	}

	public ControlObserver modifiers(Control... modifiers) {
		this.modifiers = new ArrayList<>(modifiers.length);
		for (int i = 0; i < modifiers.length; i++) {
			this.modifiers.add(modifiers[i]);
			attachListenerToModifier(this.modifiers.get(i));
		}
		return this;
	}

	public ControlObserver addCondition(Function<List<Control>, Boolean> condition) {
		this.conditions.add(condition);
		return this;
	}

	public ControlObserver childListener(List<Pane> panes) {
		this.panes = panes;
		this.panes.forEach(pane -> attachChildCountListenerToPane(pane));
		return this;
	}

	public ControlObserver childListener(Pane... panes) {
		this.panes = new ArrayList<>(panes.length);
		for (int i = 0; i < panes.length; i++) {
			this.panes.add(panes[i]);
			attachChildCountListenerToPane(panes[i]);
		}
		return this;
	}

	private void attachListenerToModifier(Control control) {
		if (control instanceof TextInputControl field) {
			field.getProperties().put(INITIAL_VALUE, field.getText());
			field.textProperty().addListener(__ -> {
				evaluate();
			});
		} else {
			logger.warning("Not supported control when attaching listener to this modifier: " +
					control.getStyleClass().stream().collect(Collectors.joining(", ")));
		}
	}

	private void attachChildCountListenerToPane(Pane pane) {
		pane.getProperties().put(INITIAL_VALUE, pane.getChildren().size());
		pane.getChildren().addListener((Change<?> change) -> {
			while (change.next()) {
				if (change.wasAdded() || change.wasRemoved())
					evaluate();
			}
		});
	}

	private void evaluate() {
		boolean shouldChangeState = true;
		List<Control> modifiablesList = Arrays.asList(modifiables);
		long passingConditions = 0;
		long modifiedControls = 0;
		long containersHavingModifiedChildCount = 0;

		if (conditions.size() > 0) {
			passingConditions = conditions.stream().filter(condition -> condition.apply(modifiablesList)).count();
			if (passingConditions != modifiablesList.size()) {
				// All conditions did not pass, hence the state will not change.
				shouldChangeState = false;
			}
		}

		if (modifiers != null && modifiers.size() > 0) {
			// Number of controls that are modified.
			modifiedControls = modifiers.stream()
					.filter(modifier -> modifier instanceof TextInputControl)
					.map(m -> (TextInputControl) m)
					.filter(field -> {
						return !field.getText().equals(field.getProperties().get(INITIAL_VALUE));
					}).count();

			logger.info("Number of modified controls: " + modifiedControls);
			logger.info("Should Change State: " + shouldChangeState);
			// If the number of modified controls is atleast 1, than the state of controls will change.
			shouldChangeState = shouldChangeState || modifiedControls > 0;
			logger.info("Should Change State: " + shouldChangeState);
		}

		// state change is determined by change in child count
		if (panes != null) {
			containersHavingModifiedChildCount = panes.stream().filter(pane -> {
				return pane.getChildren().size() != (int) pane.getProperties().get(INITIAL_VALUE);
			}).count();

			logger.info("Number of child count change: " + containersHavingModifiedChildCount);
			logger.info("Should Change State: " + shouldChangeState);
			shouldChangeState = shouldChangeState || containersHavingModifiedChildCount > 0;
			logger.info("Should Change State: " + shouldChangeState);
		}

		// TODO This requires a better understanding. The state change is always there because this evaluate method was called.
		// The shouldChangeState is not required.
		// Hence a flag is required between modifiers, child count and conditions, with a && operator.
		if (shouldChangeState) {
			ControlUtils.behavior(!enableMode, modifiablesList);
		} else {
			ControlUtils.behavior(enableMode, modifiablesList);
		}
	}

	public static <T extends Region, X extends Control> void enableOnModification(
			List<X> fields,
			Function<List<T>, Boolean> extraCondition,
			List<T> controls) {
		fields.forEach(control -> {
			if (control instanceof TextInputControl textField) {
				// Store the initial value
				textField.getProperties().put(INITIAL_VALUE, textField.getText());

				textField.textProperty().addListener(obs -> {
					Boolean conditionTrue = extraCondition.apply(controls);
					Optional<X> optModifiedControl = fields.stream().filter(f -> {
						if (f instanceof TextInputControl c) {
							return !c.getProperties().get(INITIAL_VALUE).equals(c.getText());
						}
						return false;
					}).findAny();
					if (conditionTrue && optModifiedControl.isPresent()) {
						ControlUtils.behavior(true, controls);
					} else {
						ControlUtils.behavior(false, controls);
					}
				});
			}
			// add other controls afterwards. As per requirement.
		});
	}

	public static <X extends Control> void updateInitialValues(List<X> controls) {
		controls.stream()
				.filter(f -> f instanceof TextInputControl)
				.map(m -> (TextInputControl) m)
				.forEach(control -> control.getProperties().put(INITIAL_VALUE, control.getText()));
	}

	public void updateInitialValues() {
		modifiers.stream()
				.filter(f -> f instanceof TextInputControl)
				.map(m -> (TextInputControl) m)
				.forEach(control -> control.getProperties().put(INITIAL_VALUE, control.getText()));
	}

	@SafeVarargs
	public static <X extends Control> void updateInitialValues(X... fields) {
		updateInitialValues(Arrays.asList(fields));
	}

	public static ControlObserver control(Control... controls) {
		ControlObserver obs = new ControlObserver(controls);
		return obs;
	}

	public ControlObserver enable() {
		enableMode = true;
		return this;
	}

	public ControlObserver disable() {
		enableMode = false;
		return this;
	}
}
