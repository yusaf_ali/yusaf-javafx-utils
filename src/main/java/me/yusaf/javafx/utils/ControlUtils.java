package me.yusaf.javafx.utils;

import java.util.List;

import javafx.scene.layout.Region;

/**
 * ControlUtils is mainly used for setting enabled-disabled state of any control or controls. Mainly mass action.
 */
public class ControlUtils {

	public static <T extends Region> void disable(T control) {
		control.setDisable(true);
	}

	public static <T extends Region> void enable(T control) {
		control.setDisable(false);
	}

	@SafeVarargs
	public static <T extends Region> void disable(T... controls) {
		for (T c : controls) {
			disable(c);
		}
	}

	@SafeVarargs
	public static <T extends Region> void enable(T... controls) {
		for (T c : controls) {
			enable(c);
		}
	}

	public static <T extends Region> void disable(List<T> controls) {
		for (T c : controls) {
			disable(c);
		}
	}

	public static <T extends Region> void enable(List<T> controls) {
		for (T c : controls) {
			enable(c);
		}
	}

	@SafeVarargs
	public static <T extends Region> void behavior(boolean enable, T... controls) {
		if (enable) {
			enable(controls);
		} else {
			disable(controls);
		}
	}

	public static <T extends Region> void behavior(boolean enable, List<T> controls) {
		if (enable) {
			enable(controls);
		} else {
			disable(controls);
		}
	}
}
