package me.yusaf.javafx.utils;

import java.util.logging.Logger;

import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Region;

public class CssUtils {
	private static Logger logger = Logger.getLogger("CssUtils");

	@SafeVarargs
	public static <T extends Region> void maxWidth(T... control) {
		for (T c : control) {
			maxWidth(c);
		}
	}

	@SafeVarargs
	public static <T extends Region> void maxHeight(T... control) {
		for (T c : control) {
			maxHeight(c);
		}
	}

	public static <T extends Region> T maxWidth(T control) {
		// Set Later
		control.parentProperty().addListener((obs, old, value) -> {
			control.prefWidthProperty().unbind();

			if (value == null) {
				return;
			}

			if (!(value instanceof Region)) {
				logger.warning(CssUtils.class.getSimpleName() + ": Unable to set width property of element"
						+ " [" + value + "] because it is not a region.");
				return;
			}

			maxWidth(control, (Region) value);
		});

		// Direct Set
		if (control.getParent() != null) {
			maxWidth(control, (Region) control.getParent());
		}

		return control;
	}

	public static <T extends Region> T maxHeight(T control) {
		control.parentProperty().addListener((obs, old, value) -> {
			control.prefHeightProperty().unbind();

			if (value == null) {
				return;
			}

			if (!(value instanceof Region)) {
				logger.warning(CssUtils.class.getSimpleName() + ": Unable to set height property of element"
						+ " [" + value + "] because it is not a region.");
				return;
			}

			maxHeight(control, (Region) value);
		});

		if (control.getParent() != null) {
			maxHeight(control, (Region) control.getParent());
		}

		return control;
	}

	private static <T extends Region> T maxWidth(T control, Region parent) {
		// As the padding values can not be set using control.getPadding().setLeft() like expressions,
		// adding a listener to this property will be good!
		parent.paddingProperty().addListener((obs, old, value) -> {
			maxWidthPadded(control, parent);
		});
		maxWidthPadded(control, parent);
		return control;
	}

	private static <T extends Region> T maxWidthPadded(T control, Region parent) {
		double paddingX = parent.getPadding().getLeft() + parent.getPadding().getRight();
		if (parent != null && parent.getParent() instanceof ScrollPane) {
			control.prefWidthProperty().bind(((ScrollPane) parent).prefViewportWidthProperty().subtract(paddingX));
		} else {
			control.prefWidthProperty().bind(parent.widthProperty().subtract(paddingX));
		}
		return control;
	}

	private static <T extends Region> T maxHeight(T control, Region parent) {
		// As the padding values can not be set using control.getPadding().setLeft() like expressions,
		// adding a listener to this property will be good!
		parent.paddingProperty().addListener((obs, old, value) -> {
			double paddingY = parent.getPadding().getTop() + parent.getPadding().getBottom();
			control.prefHeightProperty().bind(parent.heightProperty().subtract(paddingY));
		});

		double paddingY = parent.getPadding().getTop() + parent.getPadding().getBottom();
		control.prefHeightProperty().bind(parent.heightProperty().subtract(paddingY));

		return control;
	}

	public static void hide(Region... control) {
		toggle(false, control);
	}

	public static void show(Region... control) {
		toggle(true, control);
	}

	public static void toggle(boolean visible, Region... control) {
		for (Region region : control) {
			region.setManaged(visible);
			region.setVisible(visible);
		}
	}
}
