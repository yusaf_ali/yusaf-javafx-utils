package me.yusaf.javafx.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javafx.util.StringConverter;

public class DatePickerConverter extends StringConverter<LocalDate> {
	private static DatePickerConverter instance;
	private static DateTimeFormatter formatterDate;

	private DatePickerConverter() {
	}

	public static void init(DateTimeFormatter formatterDate) {
		DatePickerConverter.formatterDate = formatterDate;
	}

	public static DatePickerConverter getInstance() {
		if (instance == null) {
			instance = new DatePickerConverter();
		}
		return instance;
	}

	@Override
	public String toString(LocalDate date) {
		if (date != null) {
			return formatterDate.format(date);
		}
		return null;
	}

	@Override
	public LocalDate fromString(String text) {
		if (text != null && !text.isBlank()) {
			return LocalDate.parse(text, formatterDate);
		}
		return null;
	}
}
