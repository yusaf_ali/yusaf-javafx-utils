package me.yusaf.javafx.utils;

import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public class FlexUtils {
	public static void growAlways(Region... controls) {
		for (Region control : controls) {
			growAlways(control);
		}
	}

	public static void growAlways(Region control) {
		if (control.getParent() instanceof HBox) {
			HBox.setHgrow(control, Priority.ALWAYS);
		} else if (control.getParent() instanceof VBox) {
			VBox.setVgrow(control, Priority.ALWAYS);
		}
	}
}
