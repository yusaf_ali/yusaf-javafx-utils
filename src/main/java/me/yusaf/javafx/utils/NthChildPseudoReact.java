package me.yusaf.javafx.utils;

import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.css.PseudoClass;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.layout.Pane;

public class NthChildPseudoReact {
	public static void nthChildReact(Pane pane) {
		Platform.runLater(() -> {
			nthChildReact(pane.getChildren());
		});
	}

	public static void nthChildReact(Pane pane, int index) {
		removePseudoClasses(pane);
		addPseudoClass(pane, index);
		nthChildReact(pane.getChildren());
	}

	public static <E extends Node> void nthChildReact(ObservableList<E> list) {
		list.forEach(item -> {
			if (item instanceof Pane) {
				nthChildReact((Pane) item, list.indexOf(item));
			} else if (item instanceof Control) {
				removePseudoClasses(item);
				addPseudoClass(item, list.indexOf(item));
			}
		});

		list.addListener(new ListChangeListener<E>() {
			@Override
			public void onChanged(Change<? extends E> c) {
				while (c.next()) {
					if (c.wasAdded() || c.wasRemoved() || c.wasReplaced() || c.wasUpdated()) {
						list.forEach(item -> {
							removePseudoClasses(item);
							addPseudoClass(item, list.indexOf(item));
						});
					}

					if (c.wasAdded()) {
						c.getAddedSubList().forEach(addedItem -> {
							if (addedItem instanceof Pane pane) {
								nthChildReact(pane.getChildren());
							}
						});
					}
				}
			}
		});
	}

	private static void removePseudoClasses(Node pane) {
		pane.getPseudoClassStates().forEach(ps -> {
			if (ps.getPseudoClassName().startsWith("nth-child")) {
				// System.out.println("Removing PS from " + pane.getClass().getName() + ": " + ps.getPseudoClassName());
				pane.pseudoClassStateChanged(ps, false);
			}
		});
	}

	private static void addPseudoClass(Node node, int index) {
		// System.out.println("Adding nth-child-" + index + " PS [" + node.getClass().getName() + "]");
		PseudoClass nth = PseudoClass.getPseudoClass("nth-child-" + index);
		node.pseudoClassStateChanged(nth, true);
	}
}
