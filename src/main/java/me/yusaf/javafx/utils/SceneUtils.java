package me.yusaf.javafx.utils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.Window;
import me.yusaf.javafx.stylesheets.StyleSheetComponent;

/**
 * To use this utility, it is required to configure {@link #CSS_TEMP_FOLDER} and {@link #INTERNAL_RESOURCE_LOCATION}
 */
public class SceneUtils {
	private static final Logger logger = Logger.getLogger(SceneUtils.class.getName());
	private static EventHandler<KeyEvent> eventHandler;
	private static final Map<String, StyleSheetComponent> map = new HashMap<>();

	/** A path object that stores CSS files temporarily for refresh mechanism */
	public static Path CSS_TEMP_FOLDER = null;

	/**
	 * A representation of internal package structure, starting from root of owner module to the resource folder. This is expected to be a package. It is
	 * required to open resource owner packages to this module.
	 */
	public static String INTERNAL_RESOURCE_LOCATION = "";

	public static void init(Path cssTempFolder, String internalResourceLocation) throws IOException {
		CSS_TEMP_FOLDER = cssTempFolder;
		INTERNAL_RESOURCE_LOCATION = internalResourceLocation;

		Files.createDirectories(CSS_TEMP_FOLDER);
	}

	/**
	 * On each new {@link Window} or {@link Stage} object, this needs to be called.
	 */
	public static void initStylesheetReloader(Window window) {
		initStylesheetReloader(window, false);
	}

	/**
	 * On each new {@link Window} or {@link Stage} object, this needs to be called.
	 * 
	 * @param resizeOnRefresh
	 *            If enabled, window will resize according to scene.
	 */
	public static void initStylesheetReloader(Window window, boolean resizeOnRefresh) {
		Stage stg = (Stage) window;
		logger.info("Init Stylesheet Reloader for " + stg.getTitle());
		eventHandler = event -> {
			if (new KeyCodeCombination(KeyCode.F5).match(event)) {
				// Note that adding the whole map's style list to the parent from the parameters will end us up adding style sheets to a single
				// component. Which is obviously we don't want because of different CSS localizations.
				Platform.runLater(() -> {
					map.forEach((nome, c) -> {
						logger.finer("Removing CSS for " + c.name + ": [" +
								c.stylesheets.stream().collect(Collectors.joining(", ")) + "]");

						c.parent.getStylesheets().clear();
						c.stylesheetTempFiles.forEach(path -> {
							try {
								Files.deleteIfExists(path);
							} catch (IOException e) {
							}
						});
					});

					map.forEach((nome, c) -> {
						logger.finer("Loading CSS for " + c.name + ": [" +
								c.stylesheets.stream().collect(Collectors.joining(", ")) + "]");

						addStyleSheets(c, c.parent);
					});

					if (resizeOnRefresh)
						window.sizeToScene();
				});

			}
		};
		window.addEventFilter(KeyEvent.KEY_RELEASED, eventHandler);
	}

	/**
	 * Only registers provided list of style sheets to an event handler on {@link Scene} instance.<br />
	 * Does not add it when this method is called, but rather on the action of event handler.<br />
	 * Automatically prepends '/' before the names of style sheets and uses Class.getResource().toString()<br>
	 * Also adds component classes recursively to all children of this root.
	 *
	 * @param styleSheets
	 *            - Only names of the style sheets
	 * @return The event handler that was registered. Make sure to save it and when destroying the caller component, use it to unregister.
	 */
	public static void registerReloadSheets(String[] styleSheets, Parent parent, String name, Class<?> anyClass) {
		registerReloadSheets(Arrays.asList(styleSheets), parent, name, anyClass);
	}

	/**
	 * Only registers provided list of style sheets to an event handler on {@link Scene} instance.<br />
	 * Does not add it when this method is called, but rather on the action of event handler.<br />
	 * Automatically prepends '/' before the names of style sheets and uses Class.getResource().toString()<br>
	 * Also adds component classes recursively to all children of this root.
	 *
	 * @param styleSheets
	 *            Only names of the style sheets
	 * @param root
	 *            The root object that is set to the scene.
	 * @return The event handler that was registered. Make sure to save it and when destroying the caller component, use it to unregister.
	 */
	public static void registerReloadSheets(List<String> styleSheets, Parent root, String name, Class<?> anyClass) {
		logger.fine("Registering: [" + styleSheets.stream().collect(Collectors.joining(", ")) + "]");

		StyleSheetComponent c = new StyleSheetComponent();
		c.name = name;
		c.componentClass = anyClass;
		c.parent = root;
		c.imports = findImportedStyleSheets(anyClass, styleSheets);
		c.stylesheets = new ArrayList<>(styleSheets);
		c.stylesheets.addAll(c.imports);
		map.put(name, c);

		root.getStylesheets().clear();
		addStyleSheets(c, root);
		recurseAddComponentClass(root);
	}

	public static void unregisterReloadEvent(String name) {
		logger.fine("Unregister: " + name);
		map.remove(name);
	}

	/**
	 * Recursively walks through all child controls and containers and add some classes.<br>
	 * <ul>
	 * <li>Adds center, top, bottom, right and left CSS class selectors to child of BorderPane.</li>
	 * <li>Has special case handling for {@link ScrollPane} objects.</li>
	 * <li>Adds HBox to {@link HBox} and VBox to {@link VBox} objects.</li>
	 * </ul>
	 * 
	 * @param container
	 *            - The container whose children to walk through.
	 */
	public static void recurseAddComponentClass(Parent container) {
		if (container instanceof ScrollPane sp) {
			recurseAddComponentClass((Parent) sp.getContent());
		}

		if (container instanceof BorderPane bp) {
			addClassIfNotPresent(bp.getCenter(), "center");
			addClassIfNotPresent(bp.getTop(), "top");
			addClassIfNotPresent(bp.getBottom(), "bottom");
			addClassIfNotPresent(bp.getLeft(), "left");
			addClassIfNotPresent(bp.getRight(), "right");
		}

		container.getChildrenUnmodifiable().forEach(child -> {
			if (child instanceof Parent pchild) {
				recurseAddComponentClass(pchild);
			}
		});
	}

	private static void addClassIfNotPresent(Node node, String styleClass) {
		if (node != null && !node.getStyleClass().contains(styleClass)) {
			node.getStyleClass().add(styleClass);
		}
	}

	/**
	 * Adds style sheets to the specified component, does not specifically use Platform.runLater(), caller must handle that itself.<br>
	 * This will actually clear all the sheets currently attached to provided parameter 'parent'.
	 *
	 *
	 * @param styleSheets
	 * @param parent
	 * @param anyClass
	 */
	private static void addStyleSheets(StyleSheetComponent c, Parent parent) {
		c.stylesheets.forEach(ss -> {
			try {
				URL resource = c.componentClass.getResource(INTERNAL_RESOURCE_LOCATION + ss);
				List<String> lines = Files.readAllLines(Path.of(resource.toURI()));
				String content = lines.stream().filter(line -> !line.trim().startsWith("@import")).collect(Collectors.joining("\n"));

				Path newPath = CSS_TEMP_FOLDER.resolve(c.name + "_" + ss);
				Files.deleteIfExists(newPath);
				Files.createFile(newPath);
				Files.write(newPath, content.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
				c.stylesheetTempFiles.add(newPath);

				parent.getStylesheets().add(newPath.toUri().toString());
			} catch (NullPointerException e) {
				logger.severe(INTERNAL_RESOURCE_LOCATION + ss + " stylesheet was not found: [" + e.getMessage() + "]");
			} catch (FileAlreadyExistsException e) {
				logger.warning(INTERNAL_RESOURCE_LOCATION + ss + " stylesheet already exists");
			} catch (IOException | URISyntaxException e) {
				logger.severe(INTERNAL_RESOURCE_LOCATION + ss + " error with stylesheet resolving: [" + e.getMessage() + "]");
				e.printStackTrace();
			}
		});
	}

	public static void removeStylesheets(String[] stylesheets, Parent parent) {
		removeStylesheets(Arrays.asList(stylesheets), parent);
	}

	public static void removeStylesheets(List<String> stylesheets, Parent parent) {
		String ss = stylesheets.stream().collect(Collectors.joining(", "));
		logger.fine("Removing sheet: '" + ss + "' from " + parent.getClass().getName());
		parent.getStylesheets().removeIf(sheet -> {
			return stylesheets.stream().filter(s -> s.equals(sheet)).findAny().isPresent();
		});
	}

	private static Set<String> findImportedStyleSheets(Class<?> anyClass, List<String> styleSheets) {
		Set<String> list = new HashSet<>();
		styleSheets.forEach(ss -> {
			findImportedStyleSheets(anyClass, ss, list);
		});
		return list;
	}

	private static Set<String> findImportedStyleSheets(Class<?> anyClass, String ss, Set<String> list) {
		try {
			URL resource = anyClass.getResource(INTERNAL_RESOURCE_LOCATION + ss);
			if (resource == null) {
				logger.severe("Resource `" + ss + "` not found! Searching at " + INTERNAL_RESOURCE_LOCATION + ss);
				return Set.of();
			}
			Path p = Path.of(anyClass.getResource(INTERNAL_RESOURCE_LOCATION + ss).toURI());
			List<String> lines = Files.readAllLines(p);

			Pattern pattern = Pattern.compile("(@import)[\s*][\"](\\w+[.]\\w+)[\"][;]");

			Set<String> imports = lines.stream()
					.filter(line -> line.trim().startsWith("@import"))
					.map(line -> {
						try {
							Matcher m = pattern.matcher(line);
							m.matches();
							return m.group(2);
						} catch (IllegalStateException e) {
							logger.severe(e.getMessage() + " SS: " + ss + " line: " + line);
						}
						return "";
					})
					.filter(line -> !line.equals("") && !list.contains(line))
					.collect(Collectors.toSet());
			logger.finest("Found imports [" + imports.stream().collect(Collectors.joining(", ")) + "] for " + ss);

			list.addAll(imports);
			imports.forEach(im -> findImportedStyleSheets(anyClass, im, list));

			return imports;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return Set.of();
	}

	public static void clearCssDirectory() throws IOException {
		if (CSS_TEMP_FOLDER == null) {
			logger.finer("Unable to clear temp css directory. Temp css directory is not set!");
			return;
		}

		logger.finer("Clearing temp css directory: " + CSS_TEMP_FOLDER.toString());
		List<Path> list = Files.list(CSS_TEMP_FOLDER).toList();
		StringBuilder sb = new StringBuilder("Deleted: ");

		for (Path path : list) {
			sb.append(path.getFileName().toString());
			sb.append(", ");
			Files.delete(path);
		}

		if (sb.length() > 2)
			sb.setLength(sb.length() - 2);
		logger.finer(sb.toString());
	}
}
