package me.yusaf.javafx.utils;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

import javafx.beans.value.ChangeListener;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyEvent;
import me.yusaf.javafx.entity.FXBean;
import me.yusaf.javafx.tablecell.ButtonTableCell;
import me.yusaf.javafx.tablecell.CustomCheckBoxTableCell;

public class TableViewUtils {
	public static <T> TableColumn<T, Integer> indexColumn(String colName) {
		TableColumn<T, Integer> column = new TableColumn<>();
		column.getStyleClass().add("index");
		column.setCellFactory(col -> new TableCell<T, Integer>() {
			@Override
			public void updateIndex(int i) {
				super.updateIndex(i);
				setAlignment(Pos.CENTER);

				if (isEmpty() || i < 0) {
					setText(null);
				} else {
					setText((i + 1) + "");
				}
			}
		});
		return column;
	}

	/**
	 * Use<br>
	 * {@code
	 * column.setCellValueFactory(cx -> ((FXBean) cx.getValue()).checkedProperty());}
	 *
	 * @param <T>
	 * @return
	 */
	public static <T extends FXBean> TableColumn<T, Boolean> checkBoxColumn(String colName) {
		TableColumn<T, Boolean> column = new TableColumn<>(colName);
		column.setCellFactory(fx -> new CustomCheckBoxTableCell<T>());
		column.setCellValueFactory(cx -> cx.getValue().checkedProperty());
		column.setEditable(true);
		return column;
	}

	@Deprecated
	public static <T extends FXBean> TableColumn<T, Boolean> checkBoxColumn(String colName, ChangeListener<Boolean> change) {
		TableColumn<T, Boolean> c = checkBoxColumn(colName);
		c.setCellValueFactory(cx -> {
			if (change != null) {
				cx.getValue().checkedProperty().addListener(change);
			}
			return cx.getValue().checkedProperty();
		});
		return c;
	}

	public static <T extends FXBean> void setupToggleCheckOnRowClick(TableView<T> tableView) {
		setupToggleCheckOnRowClick(tableView, null);
	}

	public static <T extends FXBean> void setupToggleCheckOnRowClick(TableView<T> tableView, Consumer<T> onAction) {
		tableView.setOnMouseClicked(event -> {
			if (tableView.getSelectionModel().getSelectedCells().size() == 0) {
				return;
			}

			if (event.getTarget() instanceof TableCell<?, ?>) {
				TableCell<?, ?> cell = (TableCell<?, ?>) event.getTarget();
				if (cell != null) {
					int clickRowIndex = cell.getTableRow().getIndex();
					int rowIndex = tableView.getSelectionModel().getSelectedCells().get(0).getRow();
					if (rowIndex != clickRowIndex) {
						return;
					}
				}
			} else {
				// Sometimes, the event#getTarget returns an instance of Group, usually when event involves drag.
				// Hence, a return will simply prevent us to go there.
				// As we can't find TableCell from Group, Clicked row and selected row will not match
				return;
			}

			int rowIndex = tableView.getSelectionModel().getSelectedCells().get(0).getRow();
			T row = tableView.getItems().get(rowIndex);
			row.setChecked(!row.isChecked());
			if (onAction != null) {
				onAction.accept(row);
			}
			tableView.refresh();
		});
	}

	public static <T extends FXBean> TableColumn<T, String> buttonColumn(String colName, BiConsumer<T, Button> callback) {
		TableColumn<T, String> column = new TableColumn<>(colName);
		column.setCellFactory(cx -> new ButtonTableCell<T>(colName).onAction(callback));
		return column;
	}

	/**
	 * Make sure to also call {@link #removeSelectAll(TableView)} as this method actually puts an event filter on the scene instead of table view.<br>
	 * This will automatically remove the handler from the scene when it detects that there was a scene change.
	 * 
	 * @param tableView
	 * @param eventCtrlA
	 */
	public static void setupSelectAll(TableView<?> tableView, EventHandler<KeyEvent> eventCtrlA) {
		tableView.sceneProperty().addListener((obs, old, value) -> {
			if (old == null && value != null) {
				// A new scene
				value.addEventHandler(KeyEvent.KEY_PRESSED, eventCtrlA);
			} else if (old != null && value == null) {
				// Removed from a scene.
				old.removeEventHandler(KeyEvent.KEY_PRESSED, eventCtrlA);
			} else {
				// If the scene was merely changed, remove filter from old and put in the new one.
				old.removeEventHandler(KeyEvent.KEY_PRESSED, eventCtrlA);
				value.addEventHandler(KeyEvent.KEY_PRESSED, eventCtrlA);
			}
		});
	}

	/**
	 * Removes the selectAll handler from the scene that this table view is attached to.
	 * 
	 * @param tableView
	 * @param eventCtrlA
	 */
	public static void removeSelectAll(TableView<?> tableView, EventHandler<KeyEvent> eventCtrlA) {
		if (tableView.getScene() == null)
			return;
		Scene scene = tableView.getScene();
		scene.removeEventHandler(KeyEvent.KEY_PRESSED, eventCtrlA);
	}
}
