module yusaf_javafx_utils {
	exports me.yusaf.javafx.tablecell;
	exports me.yusaf.javafx.entity;
	exports me.yusaf.javafx.utils;
	exports me.yusaf.javafx.controls;
	exports me.yusaf.javafx.stylesheets;

	requires java.logging;
	requires transitive javafx.graphics;
	requires transitive javafx.controls;
}